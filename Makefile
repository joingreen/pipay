CONTAINER = $(shell docker ps -a -q)

c:
	docker stop $(CONTAINER) && docker rm $(CONTAINER)
b:
	docker build -t pi .
r:
	docker run -it --name pi -p 8080:3000 pi:latest
a:
	make c && make b && make r
g:
	git add . && git commit -m "17/12" && git push origin main